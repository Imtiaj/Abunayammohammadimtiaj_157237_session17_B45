<?php

namespace App\Booktitle;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Booktitle extends DB
{
    private $id;
    private $Book_title;
    private $author_Name;

    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("Booktitle", $allPostData)) {
            $this->Book_title = $allPostData['Booktitle'];
        }

        if (array_key_exists("authorName", $allPostData)) {
            $this->author_Name = $allPostData['authorName'];
        }
    }

    public function store(){
        $arrayData = array($this->Book_title, $this->author_Name);
        $query='INSERT INTO `book_title` ( `book_name`, `author_name`) VALUES (?,?)';

           $STH = $this->DBH->prepare($query);
           $result = $STH->execute($arrayData);

        if($result){
            Message::message("Data has been successfully inserted.");
        }
        else{
            Message::message("Insertion failure");
        }
        Utility::redirect('create.php');
    }
}